#define DAYS_IN_YEAR 365

enum days { SUN = 1, MON, TUE, WED, THU, FRI, SAT };

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// declare function signatures in advance in a .h file or at the the top of your
// .c file

void function1();
int function2(void);

// must declare function prototype before main
// when functions occur after your main() function
int add_two_ints(int x1, int x2); // function prototype

// Your program's entry point is a function called
// main with an integer return type.
/* int main(void) { */
/*   // your program */
/* } */

// argc arguement count being the number of arguments - your program's name
// counts as 1 argv is an array of character arrays - containing the arguments
// themselves argv[0] = name of your program, argv[1] = first argument, etc.
int main(void) {
  // printf() displays the string inside quotation
  printf("Hello, World!\n");
  printf("%d\n", 0); // print 0

  // Types

  // ints are usually 4 bytes
  int x_int = 0;
  printf("%zu\n", sizeof(x_int));

  // shorts are usually 2 bytes
  short x_short = 0;
  printf("%zu\n", sizeof(x_short));

  // chars are guaranteed to be 1 byte
  char x_char = 0;
  printf("%zu\n", sizeof(x_char));
  char y_char = 'y'; // char literals are quoted with ''
  printf("%zu\n", sizeof(y_char));

  /* longs are often 4 to 8 bytes; long longs a re guaranteed to be at least 8
   * bytes  */
  long x_long = 0;
  printf("%zu\n", sizeof(x_long));
  long long x_long_long = 0;
  printf("%zu\n", sizeof(x_long_long));

  /* floats are usually 32 bit floating point numbers */
  float x_float = 0.0f; // <- f suffix denots floating point literal
  printf("%zu\n", sizeof(x_float));

  /* doubles are usually 64 bit floating point numbers */
  double x_double = 0.0; // real numbers without any suffix are doubles
  printf("%zu\n", sizeof(x_double));

  // chars inside  single quotes are integers in machine's character set
  int zero = '0'; // => 48 in ascii character set
  printf("%d\n", zero);
  int A = 'A'; // => 65 in ascii character set

  printf("%d\n", A); // => 48 in ascii character set

  printf("%zu\n",
         sizeof(int)); // in bytes, 4 on most machines with 4 byte words

  // if the arguement of the 'sizeof' operator is an expression, then its
  // arguement is not evaluated (except VLASs (see below)) The value it yields
  // in this case is a compile time constant
  /* int a = 1; */

  // size_t is an unsigned integer type of at least 2 bytes used to represent
  // the size of an object.
  /* size_t size = sizeof(a++); */
  /* printf("sizeof(a++) = %zu where a = %d\n", size, a); */
  // prints "sizeof(a++)" = 4 where a = 1 (on 32 bit architecture)

  // arrays must be initialized with a concrete size
  char my_char_array[20]; // this array occupies 1*20 bytes
  int my_int_array[20];   // this array occupies 4* 20 bytes (80 bytes)
  // assuming 4 byte words

  size_t sizeof_my_char_array = sizeof(my_char_array);
  printf("%zu\n", sizeof_my_char_array);

  size_t sizeof_my_int_array = sizeof(my_int_array);
  printf("%zu\n", sizeof_my_int_array);

  /* you can initialize an array to 0 thusly: */
  char my_arrray[20] = {0};

  // arrays are mutable, its just memory
  my_arrray[1] = 2;
  my_arrray[2] = '3';

  for (int i = 0; i < 20; i++) {
    printf("myarray: index %d value %c\n", i, my_arrray[i]);
  };

  /* in c99 and as an optinoal feature in c11, variable lenght arrays (VLA) */
  /* can be declared as well. the size of such array need not be a compile time
   */
  /* constant */
  /* int array_size = 10; */
  /* printf("Enter an array size: "); */
  /* fscanf(stdin, "%d", &array_size); */
  int array_size = 10;
  int var_length_array[array_size]; // declare the VLA
  printf("sizeof array = %zu\n", sizeof var_length_array);

  /* eg: */
  /*  > enter the array size: 10 */
  /*   > sizeof array = 40 */

  char a_string[20] = "this is a string";
  printf("%s\n", a_string);

  printf("%d\n", a_string[16]); // byte #17 is 0 as well as 18,19,20

  // If we have characters between single quotes, that's a character literal.
  // It's of type `int`, and *not* `char` (for historical reasons).
  int cha = 'a';  // fine
  char chb = 'a'; // fine too (implicit conversion from int to char)

  printf("int: %d char: %c\n", cha, chb);

  int multi_array[2][5] = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 0}};

  int item = multi_array[0][2];
  printf("item: %d\n", item); // 3

  int i1 = 1, i2 = 2;
  float f1 = 1.0, f2 = 2.0;

  printf("Shorthand for multi declaration: %d, %d, %f, %f\n", i1, i2, f1, f2);

  int b, c;
  b = c = 0;
  printf("%d%d\n", b, c);

  printf("i1 +i2: %d\n", i1 + i2);
  printf("i1 - i2: %d\n", i1 - i2);
  printf("i1 * i2: %d\n", i1 * i2);
  printf("i1 / i2: %d\n", i1 / i2); // 0.5 but truncated towards 0

  printf("you need to cast at least one integer to float to get a floating "
         "point result\n");
  printf("%f", (float)i1 / i2);

  printf("same with double: %f\n", i1/(double)i2); // same with double

  // floating point numbers and calculations are not exact, for instance, it s not giving
  // mathematically correct results

  int works = (0.1+0.1+0.1) != 0.3; // 1 (true)

  printf("%d\n", works);

  // floating point math is not associative in c

  works = 1+ (1e123 - 1e123) != (1+1e123)-1e123; // 1 (true)

  printf("%d\n", works);

  // modulo works too, but careful if arguments are negative
  printf("%d\n", 11%3);     // 2
  printf("%d\n", (-11)%3);  // -2
  printf("%d\n", 11%(-3));  // 2

  int a = 0;

  // C is not python, comparisions do NOT chain.
  // Warning: the line below will compile, but it means `(0 < a) < 2`
  // This expression is always true, because ( <a) could either 1 or 0
  // Remeber, booleans don't exist, its integers
  // so instead of
  /* int is_between_0_and_2 = 0 < a < 2; */ // actually doesn't compile because compiler options
  // instead use:
  int better_between_0_and_2 = 0 <a && a <2;

  printf("%d\n", better_between_0_and_2);


  // Logic works on ints
  printf("!3 logical not: %d", !3);
  printf("!0 logical not: %d", !0);
  printf("!1  && !1 logical and: %d", !1 && !1);
  printf("!0  && !0 logical and: %d", !0 && !0);
  printf("!0  || !1 logical or: %d", !0 && !1);
  printf("!0  || !0 logical or: %d", !0 && !0);


  // conditional ternary expression ( ? : )
  int e = 5;
  int f = 10;
  int z;
  z = (e > f) ? e : f; // 10
  printf("z: %d\n", z);

  int j = 0;
  int s = j++; // return j then increase j
  printf("j: %d\ns: %d\n",j, s);
  s = ++j; // increment j then return j
  printf("j: %d\ns: %d\n",j, s);


  // Bitwise operators

  // ones complement bitwise negation
  printf("0x0F %d\n", 0x0F);
  printf("~0x0F %d\n", ~0x0F); // 0xFFFFFFF0
  printf("~0x0F %d\n", 0xFFFFFFF0); // 0xFFFFFFF0


  printf("0x0F %d\n", 0x0F);
  printf("0xF0 %d\n", 0xF0);
  printf("bitwise and: 0x0F & 0xF0 == 0X00 == %d\n",  0x0F & 0xF0);

  printf("bitwise or: 0x0F | 0xF0 == 0XFF == %d\n\n", 0x0F | 0xF0);

  printf("bitwise xor: 0x0F ^ 0xF0 == 0XFF == %d\n", 0x0F ^ 0xF0);
  printf("bitwise xor: 0x00 ^ 0x00 == 0X00 == %d\n", 0x00 ^ 0x00);
  printf("bitwise xor: 0xFF ^ 0xFF == 0X00 == %d\n", 0xFF ^ 0xFF);
  printf("bitwise xor: 0x0F ^ 0xFF == 0x0F == %d\n", 0x00 ^ 0x0F);
  // 100 ^ 1111 == 1011
  printf("bitwise xor: 0x04 ^ 0x0F == 0x0B == %d == 100 ^ 1111 == 1011\n\n", 0x04 ^ 0x0F);

  printf("0x01: %d\n", 0x01);
  printf("0x02: %d\n", 0x02);

  // Shifting left by N is equivalent to multiplying by 2^N.
  printf("bitwise shift left: 0x01 << 1 == 0x02 == %d\n", 0x01 << 1);

  //Shifting right by N is (if you are using ones' complement) is the equivalent of dividing by 2^N and rounding to zero.
  printf("bitwise shift left: 0x02 >> 1 == 0x01 == %d\n", 0x02 >> 1);

  /*
        Be careful when shifting signed integers - trhe following are undeefined

        - shifting into the sign bit of a signed integer

		int a = 1 << 31

		-  left shifting a negative number
	    
		int a = -1 << 2

		- shifting by an offset which is >= the width of the type of the left hand side

		int a = 1 << 32; // BU if int is 32 bits wide
   */


  // Control flow

  if(0){
	printf("never run");
  } else if (0){
	printf("never run");
  } else {
    printf("pretty basic stuff\n");
  }

  int ii = 0;

  while (ii < 10) {
	printf("%d, ", ii++);
  }

  printf("\n");


  int kk = 0;

  do {
	printf("%d, ", ++kk);
  } while (kk > 10);


  printf("\n");
  // for loops

  for (int i = 0; i < 10;i++){
	printf("%d, ", i);
  }
  printf("\n");


  // branch with multiple choices using switch



  switch(a){
  case 0:
	printf("a is 0");
	break;
  case 1:
	printf("a is 1");
	break;

  case 3:
  case 4:
	printf("a is 3 or 4");
  default:
	printf("nothing matched\n");
  }
  printf("\n");

  // typecasting

  // Every value in C has a type, but you can cast one value into another type
  // if you want (with some constraints).

  int x_hex = 0x01; // You can assign vars with hex literals

  // Casting between types will attempt to preserve their numeric values
  printf("%d\n", x_hex); // => Prints 1
  printf("%d\n", (short) x_hex); // => Prints 1
  printf("%d\n", (char) x_hex); // => Prints 1

  // Types will overflow without warning
  printf("%d\n", (unsigned char) 257); // => 1 (Max char = 255 if char is 8 bits long)


  
  // For determining the max value of a `char`, a `signed char` and an `unsigned char`,
  // respectively, use the CHAR_MAX, SCHAR_MAX and UCHAR_MAX macros from <limits.h>

  // Integral types can be cast to floating-point types, and vice-versa.
  printf("%f\n", (double) 100); // %f always formats a double...
  printf("%f\n", (float)  100); // ...even with a float.
  printf("%d\n", (char)100.0);



  // pointers

  // a pointer is a variable declared to store a memory address
  // its declaration will also tell you the type of data it points to
  // you can retrieve the memory address of your variables, then mess with them


  int x = 0;

  printf("x pointer memory address         %p\n", (void *)&x); // use & to retrieve the address of a variable


  int *pointer_x, not_a_pointer;

  pointer_x = &x;// stores the memory address of x in px
  printf("pointer_x pointer memory address %p\n", (void *)pointer_x); // => Prints some address in memory
  printf("%zu, %zu, %zu\n", sizeof((void *)&x), sizeof(pointer_x), sizeof(not_a_pointer));
  // => Prints "8, 4" on a typical 64-bit system

  // retrieve the value at tthe address a pointer is pointing to,
  // put * in front to dreference it
  // * is used to declare and deref pointers


  printf("%d\n", *pointer_x);


  // you can also change the value that a pointer is pointing too
  (*pointer_x)++;
  
  printf("pointer_x %d\n", *pointer_x); // => Prints 1
  printf("x         %d\n", x); // => Prints 1



    // Arrays are a good way to allocate a contiguous block of memory
  int x_array[20]; //declares array of size 20 (cannot change size)
  int xx;
  for (xx = 0; xx < 20; xx++) {
    x_array[xx] = 20 - xx;
  } // Initialize x_array to 20, 19, 18,... 2, 1


  /* declare a pointer of type int and initialize it to poihnt to x_array */
  int* x_ptr = x_array;

  // x_ptr now points to the first element in the array (the integer 20).
  // this works because arrays often decay into pointers to their first element

  // Exceptions:

  // when the array is the arguemenbt of the & 'address of' operator
  int arr[10];
  arr[0] = 1;
  int (*ptr_to_arr)[10] = &arr; // &arr is not of type int*
   // It's of type "pointer to array" (of ten `int`s).


  printf("%d\n", *x_ptr);
  printf("%d\n", *ptr_to_arr[0]);

  // when the array is a string literal used for initializing a char array
  /* char otherarr[] = "foobarbazquirk"; */


  // when it's the argument of the `sizeof` or `alignof` operator:
  int arraythethird[10];
  int *ptr = arraythethird; // equivalent with int *ptr = &arr[0];
  printf("%zu, %zu\n", sizeof(arraythethird), sizeof(ptr));
  // probably prints "40, 4" or "40, 8"

  //pointers are incremented and decremented based on their type
  // aka pointer arithmetic

  printf("%d\n", *(x_ptr + 1)); // => Prints 19
  printf("%d\n", x_array[1]); // => Prints 19


  // you can dynamically allocate contigous blocks of memory with sthe stdlib function malloc
  // malloc takes one arguement of type size_t
  // representing the number of bytest to allocate (usually from the heap)
  // this may not be true on embedded systems, the c standard says nothing about it


  int *my_ptr = malloc(sizeof(*my_ptr) * 20);
  for (xx = 0; xx < 20; xx++){
	*(my_ptr +xx) = 20 - xx; // my_ptr[xx] = 20 -xx
  } // initialize memory to 20,19,18.. as ints

  // be careful passing user provided values to malloc, if you want to be safe, use calloc instead
  // calloc zeros out the memory

  /* int* my_other_pointer = calloc(20, sizeof(int)); */

  // Note that there is no standard way to get the length of a dynamically allocated array in C

  // because of this, if your arrays are going to be passed around your program a lot, you need another variable to
  // keep track of the number of element (size) of an array

  // see the functions section for more info

  int size = 10;
  int *my_arr = calloc(size, sizeof(int));

  // add an element to the array
  size++;
  my_arr = realloc(my_arr, sizeof(int) * size);

  if (my_arr == NULL){
	// check for realloc failure
	return 1;
  }

  my_arr[10] = 5;

  // derefrencing memory that you havent allocated gives undefined behavior
  printf("%d\n", *(my_ptr + 21)); // => Prints who-knows-what? It may even crash.


  // when you are done with a malloc'd block of memory, you need to free it or else no one else
  // can use it until your program terminjates
  // memory leak


  free(my_ptr);


  return 0;
}
