.PHONY: run clean clean-run

main: main.c
	clang -Wall -Wextra -Werror -O2 -std=c99 -pedantic main.c -o main


run: main
	./main

clean:
	rm -f main

format:
	clang-format -i main.c

